const sonarqubeScanner = require('sonarqube-scanner');


sonarqubeScanner(
  {
    serverUrl: "https://sonarcloud.io",
    token: "3bea62fdf564338d2363c6a24e72b6686062f3af",
    options: {
      "sonar.organization": "merck-suyash-kale",
      "sonar.sources": "./src",
      "sonar.exclusions": "**/__tests__/**",
      "sonar.tests": "./src",
      "sonar.test.inclusions": "**/*.test.tsx",
      "sonar.typescript.lcov.reportPaths": "coverage/lcov.info",
      "sonar.testExecutionReportPaths": "reports/test-report.xml",
    },
  },
  () => {},
);