import React from 'react';
import ReactDOM from 'react-dom';

import App from 'App';
import 'index.css';


ReactDOM.render(
  <React.StrictMode>
    <App
      title='Hell0, React!'
      desc='Application is up and running.'
    />
  </React.StrictMode>,
  document.getElementById('root')
);