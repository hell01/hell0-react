import React from 'react';
import { render, cleanup } from "@testing-library/react";

import App from 'App';

beforeEach(cleanup);

describe('<App />', () => {

  const title = 'Hell0, React!';
  const desc = 'Hell0, React!';

  test('check title value', () => {
    const { getByTestId } = render(<App title={title} desc={desc} />);
    expect(getByTestId('title').textContent).toBe(title);
  });

});