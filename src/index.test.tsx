import React from 'react';
import ReactDOM from 'react-dom';

import App from 'App';


const title = 'Hell0, React!';
const desc = 'Hell0, React!';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App title={title} desc={desc} />, div);
  ReactDOM.unmountComponentAtNode(div);
});