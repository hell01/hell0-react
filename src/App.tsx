import React, { FC, Fragment } from 'react';

interface Props {
  title: string;
  desc: string;
};


const App: FC <Props> = ({ title, desc }) => {
  return (<Fragment>
    <h1 data-testid='title'>{title}</h1>
    {desc && <p data-testid='desc'>{desc}</p>}
  </Fragment>);
};


export default App;